class: center, middle, inverse
# Containers & <br> Virtual Machines

### B00902041 張振豪, B01902010 李紹詮
### URL: [http://w.csie.org/~b00902041/slides](http://w.csie.org/~b00902041/slides)
### June 18 / 2015

???

我們的主題是Container，上禮拜有另外一組也是報告類似的主題，
所以我們今天就盡量講些比較不一樣的部分吧！

---
class: traditional
# Reading List
.reference[
1. **Container-based operating system virtualization: a scalable, high-performance alternative to hypervisors**, Proceedings of the 2nd ACM SIGOPS/EuroSys European Conference on Computer Systems 2007, EuroSys’07
2. **Performance Evaluation of Container-Based Virtualization for High Performance Computing Environments**, 21st Euromicro International Conference on Parallel, Distributed and Network-Based Processing (PDP), 2013
3. **New Approach to Virtualization Is a Lightweight**, Computer, Volume 39, Issue 11, 2006
4. **Docker: lightweight Linux containers for consistent development and deployment**, Linux Journal, Volume 2014, Issue 239, March, 2014
]

???
這四篇是我們的指定閱讀。

第一篇是以VServer作為範例，介紹Container的各式各樣的細節。

第二篇也有提到一些Container的技術，不過這篇更著重於Performance evaluation，這包含了Computing, IO, Network, Isolation等方面的比較。

第三篇是一篇很簡短的文章，介紹了Paravirtualization以及Container兩種達成Lightweight的技術。

第四篇著重在Docker的技術與應用，不過Docker的更新相當的迅速，今年的4/16已經推出了1.6版，所以我們也有參考一些比較新的資料來向大家做介紹。

---
layout: false
name: outline
class: traditional

# Outline

0. Introduction
1. Motivation: .example[Isolation vs Efficiency]
2. Technical Highlights: .example[LXC, Docker]
3. Major experimental results
4. Main contributions
0. Comparisons: .example[Container vs Hypervisor, Container vs Container]
0. Docker: .example[Introduction & Demo]
0. Our own ideas inspred by the papers

???
這是我們今天的大綱，在第一個部分我們會向你們介紹發展比較成熟的VServer以及各式各樣Container的比較。
對於Docker的部分我們會在第二部分做介紹，因為這算是比較新的技術，所以還找不到他與其他Container技術的比較。
另外我們也會向你們帶來簡短的Docker Demo，讓你們瞭解一下他強大之處。

---
class: small-li
.left-column[
## Motivation
### PlanetLab Usage
]
.right-column[
1. [PlanetLab](http://www.planet-lab.org/) is a global research network that supports the development of new network services.
2. Many experiments and active VMs in a node.
3. Planetlab needed a solution that could maximize the performance.
![pl](./pl.png)
]
???

分佈於全球的計算機群PlanetLab項目始於2002年，目前由1337台機器組成，由694個站點託管（貢獻）。大多數機器由研究機構託管。
---
.left-column[
## Motivation
### PlanetLab Usage
### Isolation vs Efficiency
]
.right-column[
## No System Is .red[Perfect]
![tradeoff](./tradeoff.png)
]
???
Ioslation與Efficiency有著密不可分的愛恨情仇。
這張圖表我是從我們第一篇論文作者投影片抓下來的。
我們先來看看ntive系統中可以獲得最好的效能，但是這無法做到任何程序間的隔離。
而隨著隔離性的增強，代價就是效能的下降，
長久以來VMWare或者是Xen都用來當作平衡兩者的選擇，但我們相信我們可以再犧牲一點點隔離性換取接近native system的效能。
而這個方法就是所謂的Operating-system level Virtualization，也就是俗稱的容器Container。
---
.left-column[
## Motivation
### PlanetLab Usage
### Isolation vs Efficiency
### Container
]
.right-column[
![container](./container.png)
]
???
這張圖簡單說明了Container與傳統Hypervisor的不同，Container的所以有Guest Enviroment都會共享一個OS Image，來達到Performance的提升。
有許許多多的應用場景都將受惠於這樣的技術，像是先前所介紹的PlanetLab，就可以讓研究人員可以拿到更好的Performance。

---
class: inverse, middle, center

template: inverse
# Technical Highlights
### Linux-VServer as an example.

???
接下來我會以Linux-Vserver作為例子，簡單介紹一下Container是如何達到資源共享以及安全性的問題。
另外我也會稍微介紹一下在VServer中是如何盡量節省空間。


---
class: small-li
.left-column[
## Technical Hightlights
### Resource Isolation
]
.right-column[
### CPU Scheduling: Fair Share and Reservation
- Token bucket filter on top of standard O(1) Linux CPU Scheduler.

### IO QoS: Fair Share and Reservations
- HTB(Hierachical Token Bucket) of Linux `tc` is used to provide network bandwith reservations.
- Disk I/O is managed with Linux Standard Completely Fair Queueing(CFQ).

### Storage Limits
- Memory
- Disk
]

???
好，那我們先來看看VServer資源共享的機制。這部分我要介紹的是CPU, IO, Storage是如何達到公平的資源共享。
---
class: small-li
.left-column[
## Technical Hightlights
### Resource Isolation
### Security Isolation
]
.right-column[
### Process Filtering
- Hide all processes outside a VM's scope. Avoid interaction with another VM.

### Network Separation
- VServer doesn't fully virtualize the networking subsystem.
- Can't change route table entries.

### The [Chroot Barrier](http://linux-vserver.org/Secure_chroot_Barrier)
- Chroot is not secure, there are ways to break out of a chroot. 

]

???
接著讓我們來看一下一些安全性相關的問題。
---
class: small-li
.left-column[
## Technical Hightlights
### Resource Isolation
### Security Isolation
]
.right-column[
### [Upper Bound for Linux Capabilities](http://linux-vserver.org/Capabilities_and_Flags#Upper_Bound_for_Capabilities)
1. `setuid` and `setgid` is not secure.
2. It's safer to have a secure upper bound for all processes withn a context.
]
---
.left-column[
## Technical Hightlights
### Resource Isolation
### Security Isolation
### Filesystem Unification
]
.right-column[
1. Files common to more than one VM  (e.g., like libraries and binaries from similar OS distributions), can be hard linked on
a shared filesystem. 
2. The technique reduces the amount of disk space, inode caches, and even memory mappings for shared libraries.
3. VM may modify those shared files, the solution taken by VServer is to mark the files as **copy-on-write**.
]
---
class: inverse, center, middle

# Major Experimental Results
### VServer, OpenVZ, LXC, Xen
???
你們可能會想，Container效能真的有變好很多嗎?
我們第二篇的閱讀資料就有針對各式各樣的Container與傳統的Hypervisor做比較。
---
.left-column[
## Experiment
### Enviroment
]
.right-column[
1. Four identical  Dell PowerEdge R61
2. Two 2.27GHz Intel Xeon E5520 processors (with 8 cores each), 8M of L3 cache per core
3. 16GB of RAM
4. One NetXtreme II BCM5709 Gigabit Ethernet adapter
5. All nodes are inter-connected by a Dell PowerConnect 5548 Ethernet switch
6. Ubuntu 10.04 LTS with Kernel version 2.6.32-2
]

???
這是實驗上的環境，我們所要比較的四個Virtualization都是運行在相同的環境，所以不會有因為硬體問題而做出太神奇的實驗結果。
---
.left-column[
## Experiment
### Enviroment
### Computing
]
.right-column[
LINPACK: A set of fortran subroutines to solve linear equations with least square methods.
<br><img src="computing.png" class="smaller">
]
???
It consists of a set of Fortran subroutines that analyzes
and solves linear equations by the least squares method.
The LINPACK benchmark runs over a single processor and
its results can be used to estimate the performance of a
computer in the execution of CPU-intensive programs. We
ran LINPACK for matrices of order 3000 in all containerbased
system and compare them with Xen.
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
]
.right-column[
STREAM: a simple synthetic benchmark program
that measures sustainable memory bandwidth.
<br><img src="memory.png" class="smaller">
]
???
It performs
four type of vector operations (Add, Copy, Scale and Triad),
using datasets much larger than the cache memory available
in the computing environment, which reduces the waiting
time for cache misses and avoid memory reuse.
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
### Disk
]
.right-column[
IOZONE: generates and measures a variety of file operations and access patterns(such as Initial Write, Read, Re-Read and Rewrite).
<br><img src="disk.png" class="smaller">
]
???
(such as Initial Write, Read, Re-Read and Rewrite). We ran the benchmark with a file
size of 10GB and 4KB of record size.
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
### Disk
### Network
]
.right-column[
NetPIPE: a tool for measurement of network performance under a variety of conditions
<br><img src="net1.png">
]
???
It performs simple
tests such as ping-pong, sending messages of increasing
size between two processes, either through a network or a
multiprocessor architecture.
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
### Disk
### Network
]
.right-column[
<br><img src="net2.png">
]
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
### Disk
### Network
### HPC Overhead
]
.right-column[
NPB: derived from computational fluid
dynamics (CFD) applications and consist of five kernels (IS,
EP, CG, MG, FT) and three pseudo-applications (BT, SP,
LU).
<br><img src="oh1.png">
]
???
OpenMP Implementation

five kernels

IS - Integer Sort, random memory access

EP - Embarrassingly Parallel

CG - Conjugate Gradient, irregular memory access and communication

MG - Multi-Grid on a sequence of meshes, long- and short-distance communication, memory intensive

FT - discrete 3D fast Fourier Transform, all-to-all communication

three pseudo applications

BT - Block Tri-diagonal solver

SP - Scalar Penta-diagonal solver

LU - Lower-Upper Gauss-Seidel solver
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
### Disk
### Network
### HPC Overhead
]
.right-column[
<br><img src="oh2.png">
]
---
.left-column[
## Experiment
### Enviroment
### Computing
### Memory
### Disk
### Network
### HPC Overhead
### Isolation
]
.right-column[
IBS: CPU intensive test, memory
intensive test, a fork bomb, disk intensive test and two
network intensive tests (send and receive).
![isolation](isolation.png)
]
???
This set of benchmarks
includes six different stress tests: CPU intensive test, memory
intensive test, a fork bomb, disk intensive test and two
network intensive tests (send and receive).
---
class: traditional

# Main Contributions
1. Balance the tension between complete isolation of co-located VMs and efficient sharing of the physical infrastructure on which the VMs are hosted.
2. Container-based systems have a near-native performance of CPU, memory, disk and network.
3. All container-based systems are not mature yet. The only resource that could be successfully isolated was CPU.
???
接下來就讓我們來回顧一下Container到底解決了哪些問題。

首先，Container提供了一個Virtualization的選擇，讓我們能在效能與隔離性上達到更想要的平衡。

再來，在效能評估方面，讓我們瞭解到了Container-based虛擬化技術能夠達到近似於Native system Performance。

不過，在隔離性方面，我們可以發現只有CPU能夠達到完全的隔離，因此這也是一個選擇Container技術所需要考慮的一點。

---
class: traditional, center

# Comparison

|						| Hypervisor	| Container	|
| --					| --			| --		|
| Multiple Kernels		| Yes			| No 		|
| Local Administration	| Yes			| Yes		|
| Checkpoint & Resume	| Yes			| Yes		|
| Live Migration		| Yes			| Yes		|
| Live System Update	| No			| Yes		|

???
希望大家對於Container都有了基本的認識，接下來就讓我們來做些比較，看看有什麼事情是Container才可以做，而哪些事情是Hypervisor才能做。

---
class: traditional, center

# Comparison (Cont.)

| Mechanism	| OS	| Since	| Copy On Write	| Nested Virtualization	| Root Privilege Isolation	|
| --		| --	| --	| --			| --					| --						|
| Vserver	| Linux	| 2001	| Yes			| ?						| Partial 					|
| OpenVZ	| Linux	| 2005	| No			| Partial 				| Yes						|
| LXC		| Linux	| 2008	| Partial		| Yes					| Yes						|
| Docker	| Linux	| 2013	| Yes			| Yes					| No						|
| Sandboxie | Windows	| 2004	| Yes		| No					| Yes						|
<!-- | Chroot	| Unix	| 1982	| No			| Yes					| No						| -->


.footnote[
Reference: [Operating-system-level virtualization, Wikipedia](http://en.wikipedia.org/wiki/Operating-system-level_virtualization#Implementations)
]
???
這是從Wikipedia上面找到的各式各樣Container implementaion的比較，

接下來我們要介紹現在最夯的Container技術，我們就把時間交給李紹全吧!
---
class: docker, traditional

---
.left-column[
## Docker
### What is Docker
]
.right-column[
- An application deployment platform
- Based on Linux containers, supports a variety of execution drivers
![Docker Exec Drivers](./docker-execdriver-diagram.png)
]
---
.left-column[
## Docker
### What is Docker
]
.right-column[
- Creates clean containers from .example[filesystem images]
    - Runtime filesystem changes: <span class="example">Copy-on-Write</span>
    - New images can be created based on other images
- Able to .example[passthrough] specific resources
    - Host filesystem
    - Network
    - Devices
- Customize with .example[environment variables]
- Containers may be .example[linked] together for .example[autoconfiguration]
    - Linking WordPress to MySQL
]
---
.left-column[
## Docker
### What is Docker
### Docker Hub
]
.right-column[
![Docker Hub](./dockerhub.png)
]
---
.left-column[
## Docker
### What is Docker
### Docker Hub
### Usage
]
.right-column[
## `$ docker <command>`
- `search`
- `pull`
- `images`
- `run`
- `start`/`stop`
- `rm`
- `rmi`
- `build`
]
---
.left-column[
## Docker
### What is Docker
### Docker Hub
### Usage
### Dockerfile
]
.right-column[
## A short example:
```
FROM ubuntu:15.04

RUN apt-get update && apt-get install -y apache2
ADD site_root/ /var/www/html/

ENV DB_PASSWD=default_password

EXPOSE 80

VOLUME ["/var/www/html/uploads", "/var/log/apache2"]

ENTRYPOINT ["apachectl"]
CMD ["-f", "/etc/apache2/apache2.conf"]
```
#### `$ docker build -t myserver .`
#### `$ docker run --name="myserver_1" -d -e DB_PASSWD=my_passwd -v /path/to/uploads:/var/www/html/uploads -p 8080:80 myserver`
#### `$ docker stop myserver_1 && docker rm myserver_1`
]
---
.left-column[
## Docker
### What is Docker
### Docker Hub
### Usage
### Dockerfile
### Live Demo
]
.right-column[
]
---
class: traditional

# Inspiration

2. Isolation for testing (sandbox). .example[`rm -rf /`]
    - <span style="color:#aaa;">Homework in Docker...?</span>
3. Containers for mobile devices. .example[Running in root mode]

---
class: traditional

# Disadvantages

1. Everything with kernels. .example[`insmod`]
2. Live migration?
3. Security issues with namespaces/cgroups

---
class: traditional

# Additional Reference

1. [Linux VServer](http://linux-vserver.org/Welcome_to_Linux-VServer.org) official website.
2. [Docker](https://www.docker.com/) official website.
3. [Operating-system level virtualization, Wikipedia](https://en.wikipedia.org/wiki/Operating-system-level_virtualization)

---
class: inverse, center, middle

## End Of Presentation
### Any Questions Or Comments ?
